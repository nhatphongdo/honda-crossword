﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Crossword
{
    public partial class Form1 : Form
    {
        private CrosswordForm crosswordForm = null;

        public Form1()
        {
            InitializeComponent();
        }

        private void butStart_Click(object sender, EventArgs e)
        {
            Global.CurrentQuestionIndex = -1;

            if (crosswordForm != null)
            {
                crosswordForm.Close();
                crosswordForm = null;
            }

            butPickQuestion.Enabled = true;
            butQ1.Enabled = true;
            butQ2.Enabled = true;
            butQ3.Enabled = true;
            butQ4.Enabled = true;
            butQ5.Enabled = true;
            butQ6.Enabled = true;
            butQ7.Enabled = true;
            butQ8.Enabled = true;
            butQ9.Enabled = true;
            butQ10.Enabled = true;
            butQ11.Enabled = true;
            butQ12.Enabled = true;
            butQ13.Enabled = true;
            butVerticalBlink.Enabled = true;
            crosswordForm = new CrosswordForm();
            crosswordForm.Show();
        }

        private void butPickQuestion_Click(object sender, EventArgs e)
        {
            if (crosswordForm != null)
            {
                if (Global.CurrentQuestionIndex >= Global.QuestionIndexes.Length - 1)
                {
                    return;
                }

                ++Global.CurrentQuestionIndex;
                crosswordForm.SelectQuestion();

                butPickQuestion.Enabled = false;
                butDetailQues.Enabled = true;
            }
        }

        private void butRight_Click(object sender, EventArgs e)
        {
            butRight.Enabled = false;
            butWrong.Enabled = false;
            butTimer.Enabled = false;
            butPickQuestion.Enabled = true;
            if (crosswordForm != null)
            {
                crosswordForm.OpenWord(true);
            }

            if (Global.CurrentQuestionIndex >= 7)
            {
                butVerticalTimer.Enabled = true;
            }
        }

        private void butWrong_Click(object sender, EventArgs e)
        {
            butRight.Enabled = false;
            butWrong.Enabled = false;
            butTimer.Enabled = false;
            butPickQuestion.Enabled = true;
            if (crosswordForm != null)
            {
                crosswordForm.OpenWord(false);
            }

            if (Global.CurrentQuestionIndex >= 7)
            {
                butVerticalTimer.Enabled = true;
            }
        }

        private void butOpenVertical_Click(object sender, EventArgs e)
        {
            if (crosswordForm != null)
            {
                crosswordForm.OpenVertical();
                
                butPickQuestion.Enabled = false;
            }
        }

        private void butTimer_Click(object sender, EventArgs e)
        {
            if (crosswordForm != null)
            {
                crosswordForm.StartControlTimer();
                butTimer.Enabled = false;
                butRight.Enabled = true;
                butWrong.Enabled = true;
            }
        }

        private void butDetailQues_Click(object sender, EventArgs e)
        {
            if (crosswordForm != null)
            {
                crosswordForm.StartTimer();
                butDetailQues.Enabled = false;
                butTimer.Enabled = true;
                butRight.Enabled = true;
                butWrong.Enabled = true;
            }
        }

        private void butQuestion_Click(object sender, EventArgs e)
        {
            if (crosswordForm != null)
            {
                crosswordForm.StopTimer();
                crosswordForm.SelectManualQuestion(int.Parse(((Control)sender).Text));
            }
        }

        private void butVerticalBlink_Click(object sender, EventArgs e)
        {
            if (crosswordForm != null)
            {
                crosswordForm.StopTimer();
                crosswordForm.SelectVertical();
            }
        }

        private void butVerticalTimer_Click(object sender, EventArgs e)
        {
            if (crosswordForm != null)
            {
                crosswordForm.StopTimer();
                crosswordForm.StartVerticalTimer();
                butOpenVertical.Enabled = true;
            }
        }
    }
}
