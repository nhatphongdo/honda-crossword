﻿namespace Crossword
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.butStart = new System.Windows.Forms.Button();
            this.butPickQuestion = new System.Windows.Forms.Button();
            this.butRight = new System.Windows.Forms.Button();
            this.butWrong = new System.Windows.Forms.Button();
            this.butOpenVertical = new System.Windows.Forms.Button();
            this.butTimer = new System.Windows.Forms.Button();
            this.butDetailQues = new System.Windows.Forms.Button();
            this.butQ1 = new System.Windows.Forms.Button();
            this.butQ3 = new System.Windows.Forms.Button();
            this.butQ2 = new System.Windows.Forms.Button();
            this.butQ4 = new System.Windows.Forms.Button();
            this.butQ8 = new System.Windows.Forms.Button();
            this.butQ6 = new System.Windows.Forms.Button();
            this.butQ7 = new System.Windows.Forms.Button();
            this.butQ5 = new System.Windows.Forms.Button();
            this.butQ12 = new System.Windows.Forms.Button();
            this.butQ10 = new System.Windows.Forms.Button();
            this.butQ11 = new System.Windows.Forms.Button();
            this.butQ9 = new System.Windows.Forms.Button();
            this.butQ13 = new System.Windows.Forms.Button();
            this.butVerticalBlink = new System.Windows.Forms.Button();
            this.butVerticalTimer = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // butStart
            // 
            this.butStart.Location = new System.Drawing.Point(9, 10);
            this.butStart.Margin = new System.Windows.Forms.Padding(2);
            this.butStart.Name = "butStart";
            this.butStart.Size = new System.Drawing.Size(113, 42);
            this.butStart.TabIndex = 0;
            this.butStart.Text = "Bắt đầu game";
            this.butStart.UseVisualStyleBackColor = true;
            this.butStart.Click += new System.EventHandler(this.butStart_Click);
            // 
            // butPickQuestion
            // 
            this.butPickQuestion.Enabled = false;
            this.butPickQuestion.Location = new System.Drawing.Point(138, 10);
            this.butPickQuestion.Margin = new System.Windows.Forms.Padding(2);
            this.butPickQuestion.Name = "butPickQuestion";
            this.butPickQuestion.Size = new System.Drawing.Size(113, 42);
            this.butPickQuestion.TabIndex = 0;
            this.butPickQuestion.Text = "Chọn Câu Hỏi";
            this.butPickQuestion.UseVisualStyleBackColor = true;
            this.butPickQuestion.Click += new System.EventHandler(this.butPickQuestion_Click);
            // 
            // butRight
            // 
            this.butRight.Enabled = false;
            this.butRight.Location = new System.Drawing.Point(9, 132);
            this.butRight.Margin = new System.Windows.Forms.Padding(2);
            this.butRight.Name = "butRight";
            this.butRight.Size = new System.Drawing.Size(113, 42);
            this.butRight.TabIndex = 1;
            this.butRight.Text = "Đáp Án Đúng";
            this.butRight.UseVisualStyleBackColor = true;
            this.butRight.Click += new System.EventHandler(this.butRight_Click);
            // 
            // butWrong
            // 
            this.butWrong.Enabled = false;
            this.butWrong.Location = new System.Drawing.Point(138, 132);
            this.butWrong.Margin = new System.Windows.Forms.Padding(2);
            this.butWrong.Name = "butWrong";
            this.butWrong.Size = new System.Drawing.Size(113, 42);
            this.butWrong.TabIndex = 2;
            this.butWrong.Text = "Đáp Án Sai";
            this.butWrong.UseVisualStyleBackColor = true;
            this.butWrong.Click += new System.EventHandler(this.butWrong_Click);
            // 
            // butOpenVertical
            // 
            this.butOpenVertical.Enabled = false;
            this.butOpenVertical.Location = new System.Drawing.Point(138, 210);
            this.butOpenVertical.Margin = new System.Windows.Forms.Padding(2);
            this.butOpenVertical.Name = "butOpenVertical";
            this.butOpenVertical.Size = new System.Drawing.Size(113, 42);
            this.butOpenVertical.TabIndex = 3;
            this.butOpenVertical.Text = "Mở Từ Hàng Dọc";
            this.butOpenVertical.UseVisualStyleBackColor = true;
            this.butOpenVertical.Click += new System.EventHandler(this.butOpenVertical_Click);
            // 
            // butTimer
            // 
            this.butTimer.Enabled = false;
            this.butTimer.Location = new System.Drawing.Point(138, 68);
            this.butTimer.Margin = new System.Windows.Forms.Padding(2);
            this.butTimer.Name = "butTimer";
            this.butTimer.Size = new System.Drawing.Size(113, 42);
            this.butTimer.TabIndex = 0;
            this.butTimer.Text = "Bắt Đầu Đếm Giờ";
            this.butTimer.UseVisualStyleBackColor = true;
            this.butTimer.Click += new System.EventHandler(this.butTimer_Click);
            // 
            // butDetailQues
            // 
            this.butDetailQues.Enabled = false;
            this.butDetailQues.Location = new System.Drawing.Point(9, 68);
            this.butDetailQues.Name = "butDetailQues";
            this.butDetailQues.Size = new System.Drawing.Size(113, 42);
            this.butDetailQues.TabIndex = 5;
            this.butDetailQues.Text = "Chi tiết câu hỏi";
            this.butDetailQues.UseVisualStyleBackColor = true;
            this.butDetailQues.Click += new System.EventHandler(this.butDetailQues_Click);
            // 
            // butQ1
            // 
            this.butQ1.Enabled = false;
            this.butQ1.Location = new System.Drawing.Point(288, 10);
            this.butQ1.Name = "butQ1";
            this.butQ1.Size = new System.Drawing.Size(43, 42);
            this.butQ1.TabIndex = 6;
            this.butQ1.Text = "1";
            this.butQ1.UseVisualStyleBackColor = true;
            this.butQ1.Click += new System.EventHandler(this.butQuestion_Click);
            // 
            // butQ3
            // 
            this.butQ3.Enabled = false;
            this.butQ3.Location = new System.Drawing.Point(386, 10);
            this.butQ3.Name = "butQ3";
            this.butQ3.Size = new System.Drawing.Size(43, 42);
            this.butQ3.TabIndex = 7;
            this.butQ3.Text = "3";
            this.butQ3.UseVisualStyleBackColor = true;
            this.butQ3.Click += new System.EventHandler(this.butQuestion_Click);
            // 
            // butQ2
            // 
            this.butQ2.Enabled = false;
            this.butQ2.Location = new System.Drawing.Point(337, 10);
            this.butQ2.Name = "butQ2";
            this.butQ2.Size = new System.Drawing.Size(43, 42);
            this.butQ2.TabIndex = 7;
            this.butQ2.Text = "2";
            this.butQ2.UseVisualStyleBackColor = true;
            this.butQ2.Click += new System.EventHandler(this.butQuestion_Click);
            // 
            // butQ4
            // 
            this.butQ4.Enabled = false;
            this.butQ4.Location = new System.Drawing.Point(435, 10);
            this.butQ4.Name = "butQ4";
            this.butQ4.Size = new System.Drawing.Size(43, 42);
            this.butQ4.TabIndex = 8;
            this.butQ4.Text = "4";
            this.butQ4.UseVisualStyleBackColor = true;
            this.butQ4.Click += new System.EventHandler(this.butQuestion_Click);
            // 
            // butQ8
            // 
            this.butQ8.Enabled = false;
            this.butQ8.Location = new System.Drawing.Point(435, 68);
            this.butQ8.Name = "butQ8";
            this.butQ8.Size = new System.Drawing.Size(43, 42);
            this.butQ8.TabIndex = 12;
            this.butQ8.Text = "8";
            this.butQ8.UseVisualStyleBackColor = true;
            this.butQ8.Click += new System.EventHandler(this.butQuestion_Click);
            // 
            // butQ6
            // 
            this.butQ6.Enabled = false;
            this.butQ6.Location = new System.Drawing.Point(337, 68);
            this.butQ6.Name = "butQ6";
            this.butQ6.Size = new System.Drawing.Size(43, 42);
            this.butQ6.TabIndex = 10;
            this.butQ6.Text = "6";
            this.butQ6.UseVisualStyleBackColor = true;
            this.butQ6.Click += new System.EventHandler(this.butQuestion_Click);
            // 
            // butQ7
            // 
            this.butQ7.Enabled = false;
            this.butQ7.Location = new System.Drawing.Point(386, 68);
            this.butQ7.Name = "butQ7";
            this.butQ7.Size = new System.Drawing.Size(43, 42);
            this.butQ7.TabIndex = 11;
            this.butQ7.Text = "7";
            this.butQ7.UseVisualStyleBackColor = true;
            this.butQ7.Click += new System.EventHandler(this.butQuestion_Click);
            // 
            // butQ5
            // 
            this.butQ5.Enabled = false;
            this.butQ5.Location = new System.Drawing.Point(288, 68);
            this.butQ5.Name = "butQ5";
            this.butQ5.Size = new System.Drawing.Size(43, 42);
            this.butQ5.TabIndex = 9;
            this.butQ5.Text = "5";
            this.butQ5.UseVisualStyleBackColor = true;
            this.butQ5.Click += new System.EventHandler(this.butQuestion_Click);
            // 
            // butQ12
            // 
            this.butQ12.Enabled = false;
            this.butQ12.Location = new System.Drawing.Point(435, 126);
            this.butQ12.Name = "butQ12";
            this.butQ12.Size = new System.Drawing.Size(43, 42);
            this.butQ12.TabIndex = 16;
            this.butQ12.Text = "12";
            this.butQ12.UseVisualStyleBackColor = true;
            this.butQ12.Click += new System.EventHandler(this.butQuestion_Click);
            // 
            // butQ10
            // 
            this.butQ10.Enabled = false;
            this.butQ10.Location = new System.Drawing.Point(337, 126);
            this.butQ10.Name = "butQ10";
            this.butQ10.Size = new System.Drawing.Size(43, 42);
            this.butQ10.TabIndex = 14;
            this.butQ10.Text = "10";
            this.butQ10.UseVisualStyleBackColor = true;
            this.butQ10.Click += new System.EventHandler(this.butQuestion_Click);
            // 
            // butQ11
            // 
            this.butQ11.Enabled = false;
            this.butQ11.Location = new System.Drawing.Point(386, 126);
            this.butQ11.Name = "butQ11";
            this.butQ11.Size = new System.Drawing.Size(43, 42);
            this.butQ11.TabIndex = 15;
            this.butQ11.Text = "11";
            this.butQ11.UseVisualStyleBackColor = true;
            this.butQ11.Click += new System.EventHandler(this.butQuestion_Click);
            // 
            // butQ9
            // 
            this.butQ9.Enabled = false;
            this.butQ9.Location = new System.Drawing.Point(288, 126);
            this.butQ9.Name = "butQ9";
            this.butQ9.Size = new System.Drawing.Size(43, 42);
            this.butQ9.TabIndex = 13;
            this.butQ9.Text = "9";
            this.butQ9.UseVisualStyleBackColor = true;
            this.butQ9.Click += new System.EventHandler(this.butQuestion_Click);
            // 
            // butQ13
            // 
            this.butQ13.Enabled = false;
            this.butQ13.Location = new System.Drawing.Point(288, 183);
            this.butQ13.Name = "butQ13";
            this.butQ13.Size = new System.Drawing.Size(43, 42);
            this.butQ13.TabIndex = 17;
            this.butQ13.Text = "13";
            this.butQ13.UseVisualStyleBackColor = true;
            this.butQ13.Click += new System.EventHandler(this.butQuestion_Click);
            // 
            // butVerticalBlink
            // 
            this.butVerticalBlink.Enabled = false;
            this.butVerticalBlink.Location = new System.Drawing.Point(337, 183);
            this.butVerticalBlink.Name = "butVerticalBlink";
            this.butVerticalBlink.Size = new System.Drawing.Size(141, 42);
            this.butVerticalBlink.TabIndex = 18;
            this.butVerticalBlink.Text = "Hàng dọc";
            this.butVerticalBlink.UseVisualStyleBackColor = true;
            this.butVerticalBlink.Click += new System.EventHandler(this.butVerticalBlink_Click);
            // 
            // butVerticalTimer
            // 
            this.butVerticalTimer.Enabled = false;
            this.butVerticalTimer.Location = new System.Drawing.Point(9, 210);
            this.butVerticalTimer.Margin = new System.Windows.Forms.Padding(2);
            this.butVerticalTimer.Name = "butVerticalTimer";
            this.butVerticalTimer.Size = new System.Drawing.Size(113, 42);
            this.butVerticalTimer.TabIndex = 19;
            this.butVerticalTimer.Text = "Đếm Giờ Hàng Dọc";
            this.butVerticalTimer.UseVisualStyleBackColor = true;
            this.butVerticalTimer.Click += new System.EventHandler(this.butVerticalTimer_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(642, 289);
            this.Controls.Add(this.butVerticalTimer);
            this.Controls.Add(this.butVerticalBlink);
            this.Controls.Add(this.butQ13);
            this.Controls.Add(this.butQ12);
            this.Controls.Add(this.butQ10);
            this.Controls.Add(this.butQ11);
            this.Controls.Add(this.butQ9);
            this.Controls.Add(this.butQ8);
            this.Controls.Add(this.butQ6);
            this.Controls.Add(this.butQ7);
            this.Controls.Add(this.butQ5);
            this.Controls.Add(this.butQ4);
            this.Controls.Add(this.butQ2);
            this.Controls.Add(this.butQ3);
            this.Controls.Add(this.butQ1);
            this.Controls.Add(this.butDetailQues);
            this.Controls.Add(this.butOpenVertical);
            this.Controls.Add(this.butWrong);
            this.Controls.Add(this.butRight);
            this.Controls.Add(this.butTimer);
            this.Controls.Add(this.butPickQuestion);
            this.Controls.Add(this.butStart);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Crossword Controller";
            this.TopMost = true;
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button butStart;
        private System.Windows.Forms.Button butPickQuestion;
        private System.Windows.Forms.Button butRight;
        private System.Windows.Forms.Button butWrong;
        private System.Windows.Forms.Button butOpenVertical;
        private System.Windows.Forms.Button butTimer;
        private System.Windows.Forms.Button butDetailQues;
        private System.Windows.Forms.Button butQ1;
        private System.Windows.Forms.Button butQ3;
        private System.Windows.Forms.Button butQ2;
        private System.Windows.Forms.Button butQ4;
        private System.Windows.Forms.Button butQ8;
        private System.Windows.Forms.Button butQ6;
        private System.Windows.Forms.Button butQ7;
        private System.Windows.Forms.Button butQ5;
        private System.Windows.Forms.Button butQ12;
        private System.Windows.Forms.Button butQ10;
        private System.Windows.Forms.Button butQ11;
        private System.Windows.Forms.Button butQ9;
        private System.Windows.Forms.Button butQ13;
        private System.Windows.Forms.Button butVerticalBlink;
        private System.Windows.Forms.Button butVerticalTimer;
    }
}

