﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Crossword
{
    public partial class CrosswordForm : Form
    {
        private Color normalBackColor = Color.LightSkyBlue;
        private Color selectionBackColor = Color.Yellow;
        private Color verticalBackColor = Color.SeaGreen;
        private Color answeredBackColor = Color.Coral;

        private int currentSelection;
        private int selectionTimer;
        private int currentRowSelection;
        private int blinkTimer;

        private List<int> answered;

        private int timeCounter = 40;

        public CrosswordForm()
        {
            InitializeComponent();
        }

        private void CrosswordForm_Load(object sender, EventArgs e)
        {
            // Setup crossword
            answered = new List<int>();

            for (var i = 0; i < Global.Crossword.Length; i++)
            {
                for (var j = 0; j < Global.Crossword[i].Length; j++)
                {
                    if (Global.Crossword[i][j] <= 0)
                    {
                        continue;
                    }

                    var label = new Label
                                    {
                                        BorderStyle = BorderStyle.FixedSingle,
                                        Size = new Size(50, 50),
                                        Tag = string.Format("Cell_{0}_{1}", (i + 1).ToString("00"), (j + 1).ToString("00")),
                                        Font = new Font("Tahoma", 24, FontStyle.Bold),
                                        TextAlign = ContentAlignment.MiddleCenter
                                    };
                    label.Location = new Point(16 + j * (label.Size.Width - 1), 80 + i * (label.Size.Height - 1));
                    label.BackColor = Global.Crossword[i][j] == 1 ? normalBackColor : verticalBackColor;

                    panelCrossword.Controls.Add(label);
                }
            }
        }

        public void SelectQuestion()
        {
            timer5.Enabled = false;

            if (Global.CurrentQuestionIndex >= Global.QuestionIndexes.Length)
            {
                return;
            }

            lblTimer.Text = string.Empty;
            selectionTimer = 0;
            currentSelection = 0;
            do
            {
                ++currentSelection;
                if (currentSelection > Global.QuestionIndexes.Length)
                {
                    currentSelection = 1;
                }
            }
            while (answered.Contains(currentSelection));

            timer2.Enabled = true;
        }

        public void SelectManualQuestion(int rowSelected)
        {
            if (rowSelected < 1 || rowSelected > Global.QuestionIndexes.Length)
            {
                return;
            }

            blinkTimer = 0;
            lblTimer.Text = string.Empty;
            currentRowSelection = rowSelected;
            timer3.Enabled = true;
            timer4.Enabled = false;
        }

        public void SelectVertical()
        {
            blinkTimer = 0;
            lblTimer.Text = string.Empty;
            timer4.Enabled = true;
            timer3.Enabled = false;
        }

        public void StartTimer()
        {
            timer2.Enabled = false;

            var index = Global.QuestionIndexes[Global.CurrentQuestionIndex];
            lblHeadQuestion.Text = Global.HeadQuestions[index - 1];

            lblQuestion1.Text = string.Empty;
            lblQuestion2.Text = string.Empty;
            lblQuestion3.Text = string.Empty;
            picQuestion.BackgroundImage = null;

            for (var i = 0; i < Global.Questions[index - 1].Length; i++)
            {
                if (Global.AppearTimes[index - 1][i] > 0)
                {
                    continue;
                }

                switch (i)
                {
                    case 0:
                        lblQuestion1.Text = Global.Questions[index - 1][i];
                        break;
                    case 1:
                        lblQuestion2.Text = Global.Questions[index - 1][i];
                        break;
                    case 2:
                        lblQuestion3.Text = Global.Questions[index - 1][i];
                        break;
                }
            }

            if (Global.AppearTimes[index - 1].Length > Global.Questions[index - 1].Length && Global.AppearTimes[index - 1][Global.Questions[index - 1].Length] == 0)
            {
                if (!string.IsNullOrEmpty(Global.Images[index - 1]))
                {
                    picQuestion.BackgroundImage = Image.FromFile(Global.Images[index - 1]);
                }
            }

            panelCrossword.Visible = false;
            panelQuestion.Visible = true;

            //lblTimer.Text = timeCounter.ToString();
            //timer1.Enabled = true;
        }

        public void StartControlTimer()
        {
            lblTimer.Text = timeCounter.ToString();
            timer1.Enabled = true;
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            var time = int.Parse(lblTimer.Text);
            --time;
            if (time < 0)
            {
                timer1.Enabled = false;
            }
            else
            {
                if (time % 20 == 0)
                {
                    panelCrossword.Visible = true;
                    panelQuestion.Visible = false;
                }
                else if (time % 20 == 17)
                {
                    panelCrossword.Visible = false;
                    panelQuestion.Visible = true;
                }

                lblTimer.Text = time.ToString();

                var index = Global.QuestionIndexes[Global.CurrentQuestionIndex];
                for (var i = 0; i < Global.Questions[index - 1].Length; i++)
                {
                    if ((timeCounter - time) < Global.AppearTimes[index - 1][i])
                    {
                        continue;
                    }

                    switch (i)
                    {
                        case 0:
                            lblQuestion1.Text = Global.Questions[index - 1][i];
                            break;
                        case 1:
                            lblQuestion2.Text = Global.Questions[index - 1][i];
                            break;
                        case 2:
                            lblQuestion3.Text = Global.Questions[index - 1][i];
                            break;
                    }
                }

                if (Global.AppearTimes[index - 1].Length > Global.Questions[index - 1].Length &&
                    (timeCounter - time) >= Global.AppearTimes[index - 1][Global.Questions[index - 1].Length])
                {
                    if (!string.IsNullOrEmpty(Global.Images[index - 1]))
                    {
                        picQuestion.BackgroundImage = Image.FromFile(Global.Images[index - 1]);
                    }
                }
            }
        }

        private void timer2_Tick(object sender, EventArgs e)
        {
            selectionTimer += timer2.Interval;

            foreach (var c in panelCrossword.Controls)
            {
                if (!(c is Label))
                {
                    continue;
                }

                var control = (Label)c;
                if (control.Tag == null)
                {
                    continue;
                }

                var tagParts = control.Tag.ToString().Split('_');

                if (tagParts[0] == "Cell")
                {
                    var row = int.Parse(tagParts[1]);
                    var column = int.Parse(tagParts[2]);

                    if (row == currentSelection)
                    {
                        control.BackColor = selectionBackColor;
                    }
                    else if (!answered.Contains(row))
                    {
                        control.BackColor = Global.Crossword[row - 1][column - 1] == 1 ? normalBackColor : verticalBackColor;
                    }
                }
            }

            if (selectionTimer > 2000 && currentSelection == Global.QuestionIndexes[Global.CurrentQuestionIndex])
            {
                timer2.Enabled = false;
            }
            else
            {
                do
                {
                    ++currentSelection;
                    if (currentSelection > Global.QuestionIndexes.Length)
                    {
                        currentSelection = 1;
                    }
                }
                while (answered.Contains(currentSelection));
            }
        }

        public void OpenWord(bool right)
        {
            timer1.Enabled = false;

            answered.Add(Global.QuestionIndexes[Global.CurrentQuestionIndex]);

            panelCrossword.Visible = true;
            panelQuestion.Visible = false;

            var minIndex = Global.Crossword[Global.QuestionIndexes[Global.CurrentQuestionIndex] - 1].ToList().FindIndex(i => i > 0);

            foreach (var c in panelCrossword.Controls)
            {
                if (!(c is Label))
                {
                    continue;
                }

                var control = (Label)c;
                if (control.Tag == null)
                {
                    continue;
                }

                var tagParts = control.Tag.ToString().Split('_');

                if (tagParts[0] == "Cell")
                {
                    var row = int.Parse(tagParts[1]);
                    var column = int.Parse(tagParts[2]);

                    if (row == Global.QuestionIndexes[Global.CurrentQuestionIndex])
                    {
                        if (right == true)
                        {
                            control.Text = Global.Answers[row - 1][column - 1 - minIndex].ToString();
                            currentRowSelection = row;
                            blinkTimer = 0;
                            timer3.Enabled = true;
                        }
                        control.BackColor = Global.Crossword[row - 1][column - 1] == 1 ? answeredBackColor : verticalBackColor;
                    }
                }
            }
        }

        public void OpenVertical()
        {
            timer1.Enabled = false;
            timer2.Enabled = false;
            timer5.Enabled = false;

            panelCrossword.Visible = true;
            panelQuestion.Visible = false;

            foreach (var c in panelCrossword.Controls)
            {
                if (!(c is Label))
                {
                    continue;
                }

                var control = (Label)c;
                if (control.Tag == null)
                {
                    continue;
                }

                var tagParts = control.Tag.ToString().Split('_');

                if (tagParts[0] == "Cell")
                {
                    var row = int.Parse(tagParts[1]);
                    var column = int.Parse(tagParts[2]);

                    if (Global.Crossword[row - 1][column - 1] == 2)
                    {
                        var minIndex = Global.Crossword[row - 1].ToList().FindIndex(i => i > 0);
                        control.Text = Global.Answers[row - 1][column - 1 - minIndex].ToString();
                        SelectVertical();
                    }
                }
            }
        }

        private void timer3_Tick(object sender, EventArgs e)
        {
            // Blink
            blinkTimer += timer3.Interval;

            foreach (var c in panelCrossword.Controls)
            {
                if (!(c is Label))
                {
                    continue;
                }

                var control = (Label)c;
                if (control.Tag == null)
                {
                    continue;
                }

                var tagParts = control.Tag.ToString().Split('_');

                if (tagParts[0] == "Cell")
                {
                    var row = int.Parse(tagParts[1]);
                    var column = int.Parse(tagParts[2]);

                    var currentBackColor = Global.Crossword[row - 1][column - 1] == 1 ? (answered.Contains(row) ? answeredBackColor : normalBackColor) : verticalBackColor;
                    if (row == currentRowSelection)
                    {
                        if (blinkTimer >= 3000)
                        {
                            control.BackColor = currentBackColor;
                        }
                        else
                        {
                            control.BackColor = (control.BackColor == currentBackColor ? selectionBackColor : currentBackColor);
                        }
                    }
                    else
                    {
                        control.BackColor = currentBackColor;
                    }
                }
            }

            if (blinkTimer >= 3000)
            {
                timer3.Enabled = false;
            }
        }

        private void timer4_Tick(object sender, EventArgs e)
        {
            // Blink
            blinkTimer += timer4.Interval;

            foreach (var c in panelCrossword.Controls)
            {
                if (!(c is Label))
                {
                    continue;
                }

                var control = (Label)c;
                if (control.Tag == null)
                {
                    continue;
                }

                var tagParts = control.Tag.ToString().Split('_');

                if (tagParts[0] == "Cell")
                {
                    var row = int.Parse(tagParts[1]);
                    var column = int.Parse(tagParts[2]);

                    var currentBackColor = Global.Crossword[row - 1][column - 1] == 1 ? (answered.Contains(row) ? answeredBackColor : normalBackColor) : verticalBackColor;
                    if (Global.Crossword[row - 1][column - 1] == 2)
                    {
                        if (blinkTimer >= 3000)
                        {
                            control.BackColor = verticalBackColor;
                        }
                        else
                        {
                            control.BackColor = (control.BackColor == verticalBackColor ? selectionBackColor : verticalBackColor);
                        }
                    }
                    else
                    {
                        control.BackColor = currentBackColor;
                    }
                }
            }

            if (blinkTimer >= 3000)
            {
                timer4.Enabled = false;
            }
        }

        public void StartVerticalTimer()
        {
            lblTimer.Text = timeCounter.ToString();
            timer5.Enabled = true;
        }

        private void timer5_Tick(object sender, EventArgs e)
        {
            var time = int.Parse(lblTimer.Text);
            --time;
            if (time < 0)
            {
                timer5.Enabled = false;
            }
            else
            {
                lblTimer.Text = time.ToString();
            }
        }

        public void StopTimer()
        {
            timer1.Enabled = false;
            timer2.Enabled = false;
            timer3.Enabled = false;
            timer4.Enabled = false;
        }
    }
}

