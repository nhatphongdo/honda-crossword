﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Crossword
{
    static class Global
    {
        public static string[] HeadQuestions = new string[]
                                                   {
                                                       "Dựa vào những gợi ý sau đây, em hãy cho biết Ô chữ này là gì?",
                                                       "Dựa vào những gợi ý sau đây, em hãy cho biết Ô chữ này là gì?",
                                                       "Dựa vào những gợi ý sau đây, em hãy cho biết Ô chữ này là gì?",
                                                       "Loại biển báo nào có dạng hình Tam giác đều viền đỏ, nền vàng, hình vẽ màu đen?",
                                                       "Em hãy điền từ còn thiếu vào ô trống sau:",
                                                       "Dựa vào những gợi ý sau đây, em hãy cho biết Ô chữ này là gì?",
                                                       "Dựa vào những gợi ý sau đây, em hãy cho biết đây là nơi nào?",
                                                       "Em hãy điền từ còn thiếu vào câu dưới đây?",
                                                       "Em hãy cho biết, đèn xanh, đèn đỏ, đèn vàngđược gọi chung là gì?",
                                                       "Trong tranh dưới đây, tầm nhìn của bạn nhỏ đi xe đạp bị che khuất bởi vật gì?",
                                                       "Em hãy cho biết tên của biển báo dưới đây với gợi ý: đây là một biển báo nguy hiểm",
                                                       "Em hãy cho biết tên của vật này:\n- Khi ngồi trong xe ô tô , để đảm bảo an toàn các em nên “thắt” vật này",
                                                       "Em hãy cho biết:\nĐể bảo vệ vùng đầu, giảm nguy cơ chấn thương sọ não khi các em đi trên đường và không may xảy ra tai nạn giao thông, các em cần đội vật gì?"
                                                   };

        public static string[][] Questions = new string[][]
                                                 {
                                                     new string[]
                                                         {
                                                             "Đây là việc cần làm trước khi đi xe đạp để đảm bảo an toàn.",
                                                             "Mục đích của việc này là để đảm bảo mọi bộ phận của xe đều hoạt động tốt, nhất là phanh, chuông và lốp xe"
                                                         },
                                                     new string[]
                                                         {
                                                             "Đây là tên gọi chung của xe đạp (kể cả xe đạp máy), xe xích lô, xe lăn dùng cho người khuyết tật, xe súc vật kéo và các loại xe tương tự."
                                                         },
                                                     new string[]
                                                         {
                                                             "Đây là tên nhân vật đồng hành với các Em ở mỗi bài học trong sách ATGT cho nụ cười trẻ thơ.",
                                                             "Nhân vật này đã từng sang Việt Nam năm 2008",
                                                             "Đây là hình ảnh nhân vật này trong mỗi bài học"
                                                         },
                                                     new string[] {},
                                                     new string[]
                                                         {
                                                             "Ở những nơi không có hè phố, lề đường, các em hãy đi sát mép đường ........ để tránh các xe đang chạy trên đường"
                                                         },
                                                     new string[]
                                                         {
                                                             "Đây là thời điểm các phương tiện giao thông phải bật đèn khi đi",
                                                             "Đây là thời điểm trong ngày không có ánh sáng"
                                                         },
                                                     new string[]
                                                         {
                                                             "Đây là một trong những nơi vui chơi an toàn",
                                                             "Nơi này có rất nhiều cây xanh và hoa",
                                                             "Đây là hình ảnh thực tế của nơi này"
                                                         },
                                                     new string[]
                                                         {
                                                             "Để đảm bảo an toàn khi đi xe vào buổi tối, em cần lắng nghe tiếng ........ và chú ý đèn xe để nhận biết xe đang tới"
                                                         },
                                                     new string[] {},
                                                     new string[] {},
                                                     new string[] {},
                                                     new string[] {},
                                                     new string[] {}
                                                 };

        public static string[] Images = new string[]
                                            {
                                                "",
                                                "Picture1.png",
                                                "Picture2.jpg",
                                                "Picture3.png",
                                                "",
                                                "",
                                                "Picture4.jpg",
                                                "",
                                                "",
                                                "Picture5.png",
                                                "Picture6.jpg",
                                                "Picture7.jpg",
                                                ""
                                            };

        public static int[][] AppearTimes = new int[][]
                                                {
                                                    new int[]
                                                        {
                                                            0, 0
                                                        },
                                                    new int[]
                                                        {
                                                            0, 0
                                                        },
                                                    new int[]
                                                        {
                                                            0, 10, 30, 30
                                                        },
                                                    new int[]
                                                        {
                                                            0
                                                        },
                                                    new int[]
                                                        {
                                                            0
                                                        },
                                                    new int[]
                                                        {
                                                            0, 0
                                                        },
                                                    new int[]
                                                        {
                                                            0, 10, 30, 30
                                                        },
                                                    new int[]
                                                        {
                                                            0
                                                        },
                                                    new int[]
                                                        {
                                                        },
                                                    new int[]
                                                        {
                                                            0
                                                        },
                                                    new int[]
                                                        {
                                                            0
                                                        },
                                                    new int[]
                                                        {
                                                            0
                                                        },
                                                    new int[]
                                                        {
                                                        }
                                                };

        public static int[] QuestionIndexes = new int[]
                                                  {
                                                      1, 13, 3, 2, 10, 7, 11, 4, 8, 12, 5, 6, 9
                                                  };

        public static string[] Answers = new string[]
                                             {
                                                 "KIỂMTRAXE",
                                                 "XETHÔSƠ",
                                                 "ASIMO",
                                                 "NGUYHIỂM",
                                                 "BÊNPHẢI",
                                                 "BUỔITỐI",
                                                 "CÔNGVIÊN",
                                                 "CÒIXE",
                                                 "TÍNHIỆUĐÈN",
                                                 "BỨCTƯỜNG",
                                                 "CÔNGTRƯỜNG",
                                                 "DÂYANTOÀN",
                                                 "MŨBẢOHIỂM"
                                             };

        public static int[][] Crossword = new int[][]
                                              {
                                                  new int[]
                                                      {
                                                          0, 1, 1, 1, 1, 2, 1, 1, 1, 1, 0, 0, 0, 0
                                                      },
                                                  new int[]
                                                      {
                                                          0, 1, 1, 1, 1, 2, 1, 1, 0, 0, 0, 0, 0, 0
                                                      },
                                                  new int[]
                                                      {
                                                          0, 0, 0, 1, 1, 2, 1, 1, 0, 0, 0, 0, 0, 0
                                                      },
                                                  new int[]
                                                      {
                                                          0, 0, 1, 1, 1, 2, 1, 1, 1, 1, 0, 0, 0, 0
                                                      },
                                                  new int[]
                                                      {
                                                          0, 0, 0, 0, 1, 2, 1, 1, 1, 1, 1, 0, 0, 0
                                                      },
                                                  new int[]
                                                      {
                                                          0, 0, 0, 0, 1, 2, 1, 1, 1, 1, 1, 0, 0, 0
                                                      },
                                                  new int[]
                                                      {
                                                          0, 1, 1, 1, 1, 2, 1, 1, 1, 0, 0, 0, 0, 0
                                                      },
                                                  new int[]
                                                      {
                                                          0, 0, 0, 1, 1, 2, 1, 1, 0, 0, 0, 0, 0, 0
                                                      },
                                                  new int[]
                                                      {
                                                          1, 1, 1, 1, 1, 2, 1, 1, 1, 1, 0, 0, 0, 0
                                                      },
                                                  new int[]
                                                      {
                                                          0, 0, 1, 1, 1, 2, 1, 1, 1, 1, 0, 0, 0, 0
                                                      },
                                                  new int[]
                                                      {
                                                          0, 0, 0, 1, 1, 2, 1, 1, 1, 1, 1, 1, 1, 0
                                                      },
                                                  new int[]
                                                      {
                                                          0, 0, 1, 1, 1, 2, 1, 1, 1, 1, 1, 1, 0, 0
                                                      },
                                                  new int[]
                                                      {
                                                          0, 0, 0, 0, 0, 2, 1, 1, 1, 1, 1, 1, 1, 1
                                                      }
                                              };

        public static int CurrentQuestionIndex = -1;
    }
}
